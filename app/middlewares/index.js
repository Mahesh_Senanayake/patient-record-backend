const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const verifyPatientSignUp = require("./verifySignUp");
module.exports = {
  authJwt,
  verifySignUp,
  verifyPatientSignUp,
};

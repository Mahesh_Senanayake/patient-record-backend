const db = require("../models");
const ROLES = db.ROLES;
const Patient = db.patient;
checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Nicnumber
  Patient.findOne({
    nicnumber: req.body.nicnumber,
  }).exec((err, patient) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (patient) {
      res
        .status(400)
        .send({ message: "Failed! NIC Number is already in use!" });
      return;
    }
    // Email
    Patient.findOne({
      email: req.body.email,
    }).exec((err, patient) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (patient) {
        res.status(400).send({ message: "Failed! Email is already in use!" });
        return;
      }
      next();
    });
  });
};
checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Failed! Role ${req.body.roles[i]} does not exist!`,
        });
        return;
      }
    }
  }
  next();
};
const verifyPatientSignUp = {
  checkDuplicateUsernameOrEmail,
  checkRolesExisted,
};
module.exports = verifyPatientSignUp;

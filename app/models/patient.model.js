const mongoose = require("mongoose");
const Patient = mongoose.model(
  "Patient",
  new mongoose.Schema({
    firstname: String,
          lastname: String,
          nicnumber: String,
          address: String,
          email: String,
          phonenumber: String,
          dateofbirth: Date,
          gender: String,
          height: String,
          weight: String,
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
      },
    ],
  })
);
module.exports = Patient;
